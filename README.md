# Book recommender app, pet project

## Data used

Using dataset [goodbooks-10k](https://github.com/zygmuntz/goodbooks-10k). It contains 6 mio raitings for 10 thousand most popular books.

## Workflow

1. [model_development.ipynb](model_development.ipynb) - reading data and developing model and saving pretrained model and embeddings
2. [prototype.py](prototype.py) - making web app to return recommendations - using pretrained models

## How to run app

```bash
streamlit run prototype.py
```

## To do

- [ ] add screenshots
- [ ] add requirements
- [ ] create docker container