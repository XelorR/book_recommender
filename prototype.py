import streamlit as st
import pandas as pd
import nmslib
import pickle


def nearest_books_nms(book_id, index, n=10):
    """
    Finding nearest neighbors, returning index
    """
    nn = index.knnQuery(item_embeddings[book_id], k=n)
    return nn


def get_names(index):
    """
    Returns book names
    
    input - idx of books
    return - list of names
    """
    names = []
    for idx in index:
        names.append(
            "Book name:  {} ".format(name_mapper[idx])
            + "  Book Author: {}".format(author_mapper[idx])
        )
    return names


def read_files(folder_name="data"):
    """
    Reading and lowercasing files
    """
    ratings = pd.read_csv(folder_name + "/ratings.csv")
    books = pd.read_csv(folder_name + "/books.csv")
    books["title"] = books.title.str.lower()
    return ratings, books


def make_mappers():
    """
    Reflects id to title
    """
    name_mapper = dict(zip(books.book_id, books.title))
    author_mapper = dict(zip(books.book_id, books.authors))

    return name_mapper, author_mapper


def load_embeddings():
    """
    Loading embeddings from disk
    """
    with open("item_embeddings.pkl", "rb") as f:
        item_embeddings = pickle.load(f)

    # Using nmslib to create or quick knn
    nms_idx = nmslib.init(method="hnsw", space="cosinesimil")
    nms_idx.addDataPointBatch(item_embeddings)
    nms_idx.createIndex(print_progress=True)
    return item_embeddings, nms_idx


if __name__ == "__main__":

    st.title("Books recommender app")

    # loading data
    ratings, books = read_files(folder_name="data")
    name_mapper, author_mapper = make_mappers()
    item_embeddings, nms_idx = load_embeddings()

    # user form for text input
    title = st.text_input("Book name part to search for", "")
    title = title.lower()

    # filtering books
    output = books[books.title.str.contains(title) > 0]

    # swlwcring book from the list
    option = st.selectbox("Select the exact one from the list", output["title"].values)

    # returning book selection
    "You selected: ", option

    books_count_to_return = st.slider("How many recommendations do you want to see?", 1, 15, 5)

    # finding recommendations
    val_index = output[output["title"].values == option].book_id
    index = nearest_books_nms(val_index, nms_idx, books_count_to_return+1)

    # returning recommendations
    "Most similar books are: "
    st.write("", get_names(index[0])[1:])
